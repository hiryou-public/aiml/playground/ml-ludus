.. raw:: html

    <style>
        main {
            width: 950px;
        }

        div.holder {
            padding-left: 0;
            margin-left: -35px;
            overflow: hidden;
        }
        div.leftside {
            padding: 0px;
            float: left;
        }
        div.rightside {
            margin-left: -20px;
            float: left;
            width: 400px;
        }

        div.math-legends li > p > .formula:first-child {
            display: inline-block;
            width: 120px;
            text-align: left;
            margin-top: 0;
            margin-bottom: 0;
        }

        span.formula {
            font-size: 18px;
        }
        span.formula > span {
            margin-right: 8px;
        }

        .marker {color:#A0C5AC; float: right}

        .emphaline {
            text-decoration: underline;
            font-style: italic;
        }
    </style>

.. role:: marker
.. role:: emphaline

----

4.2.2 Initializing the Weights
------------------------------

.. container:: holder

   .. container:: leftside

      .. figure:: static/forw-neurons.jpg
         :width: 400

   .. container:: rightside

        Number of input neurons to neuron A: :math:`N = 3`

        Input to neuron A:

        - Linear sum: :math:`h = \sum_{i=1}^{3}{w_i x_i} = w_1x_1 + w_2x_2 + w_3x_3` :marker:`(2)`
        - Sigmoid activation: :math:`g(h) = \frac{1}{1 + e^{-x}}` :marker:`(3)`

        *Weights are often initialized to* :math:`[ -\frac{1}{\sqrt{N}} , \frac{1}{\sqrt{N}} ]` - WHY? :marker:`(4)`

----

This method is called ``Xavier Initialization``. Xavier is just 1 of several methods to randomize neuron weights before
training. The particular range ``(4)`` was used for sigmoid and standard normal distribution weight sampling. For other
activation functions and distributions, the chosen ranges can be different.

A great source of explanation is from
`andyljones's blogpost <https://andyljones.tumblr.com/post/110998971763/an-explanation-of-xavier-initialization>`_

1. How to arrive at range ``(4)`` when using sigmoid activation?

    Statistics revision `source: wiki <https://en.wikipedia.org/wiki/Variance#Product_of_independent_variables>`_ :

        If 2 variables X and Y are independent, then:

            :math:`Var(XY) = [E(X)]^2Var(Y) + [E(Y)]^2Var(x) + Var(x)Var(Y)` ``(variance of product)``

            :math:`Var(X + Y) = Var(x) + Var(Y)` ``(variance of sum)``

        where :math:`E[X]` reads: expected value of variable X

        if X also has zero-mean :math:`\bar{X} = 0`, then :math:`E[X] = 0`

    Wrt formula ``(2)``, any term :math:`W_i X_i` has the following variance:
        :math:`Var(W_i X_i) = [E(X_i)]^2Var(W_i) + [E(W_i)]^2Var(X_i) + Var(X_i)Var(W_i)`

    If X and W are both independent, :math:`\bar{X} = 0` and :math:`\bar{W} = 0`, then :math:`E(X_i) = 0` and
    :math:`E(W_i) = 0`, :math:`Var(W_i X_i)` is simplified to:

        :math:`Var(W_i X_i) = Var(X_i)Var(W_i)`

    From here, variance of ``h`` in ``(2)`` is computed as:
        :math:`Var(h) = Var(w_1x_1) + Var(w_2x_2) + Var(w_3x_3) = N Var(W) * Var(X)`, :marker:`(5)`

        `N = 3` in this

    Because we want :math:`Var(h) = Var(X)`, this condition must satisify: :math:`N*Var(W) = 1`, implies:
        :math:`Var(W) = \frac{1}{N}` :marker:`(6)`

        hence, :math:`W_i \in [ -\frac{1}{\sqrt{N}} , \frac{1}{\sqrt{N}} ]` :marker:`(Q.E.D)`

    In Pytorch ANN impl, init weights also follow this rule, see
    `source <https://stackoverflow.com/questions/49816627/what-is-the-default-weight-initializer-for-conv-in-pytorch>`_

2. In words, now we see why it's important to:

    a. Choose features that are independent from one another. This help make all the terms in ``(2)`` independent.

    b. Scale every feature :math:`X_i` to a a specific distribution with :math:`Var(X) = 1, \bar{X} = 0`.

    As all input values started in range :math:`[-1, 1]`, condition ``(Q.E.D)`` ensures every neuron linear output in the
    next layer also lies in :math:`[-1, 1]`, and sigmoid for any input in this range is small enough and lies in
    :math:`(-1, 1)`. As a result, linear input to any neuron across al layers remain in a small reasonable range, allow
    sigmoid activation to make effective weight update.

3.  Why choosing the range :math:`[-1, 1]` for effective weight update?

    This we have to look at the shape of sigmoid function
    `image source <https://mathworld.wolfram.com/images/eps-svg/SigmoidFunction_701.svg>`_:

    .. container:: holder

       .. container:: leftside

          .. figure:: static/sigmoid-shape.png

       .. container:: rightside

            x-axis represents linear input to a neuron;

            y-axis gives the relevant sigmoid output;

    When the linear input :math:`\in [-1, 1]`, its sigmoid shape is almost linear and has significant
    derivative (gradient vector). This means a gradient descent (moving in the steepest direction) is effective, making
    a weight update meaningful. If linear input is too large (< -4 or > 4), the sigmoid shape is saturated (almost flat out)
    at these positions. So a gradient descent is very small, making it move very slowly. This translates to slow
    and costly leaning.

----

While ``Xavier Initialization`` only depends on # of input neurons, ``Glorot initialization`` is another method which
takes into account # of both input & output neurons. The argument applies in the back-propagation step.

.. container:: holder

   .. container:: leftside

      .. figure:: static/back-neurons.jpg
         :width: 400

   .. container:: rightside

        Number of output neurons from neuron B: :math:`M = 2`

        Let's ``L`` denote the loss function, set :math:`\Delta_{y1} = \frac{\delta_L}{\delta_{y_1}}` (derivative of loss with
        respect to y1), :math:`\Delta_{B} = \frac{\delta_L}{\delta_B}` (derivative of loss with respect to B). Thanks to an
        already proven math, we have:

            :math:`\Delta_{B} = \sum_{i=1}^{2}{w_i \Delta_{y_i}} = w_1 \Delta_{y_1} + w_2 \Delta_{y_2}` :marker:`(7)`

Formula ``(7)`` is similar to ``(2)``. As how we go from ``(2)`` to ``(5)``, we can also go from ``(7)`` to:
    :math:`Var(\Delta_{B}) = Var(w_1\Delta_{y_1}) + Var(w_2\Delta_{y_2}) = M Var(W) * Var(\Delta_y)`, `M = 2` in this
    example ``(8)``

In back-propagation, it's argued that `we should maintain change to input & output neurons on the same range`,
which means we want:

    :math:`Var(\Delta_{B}) = Var(\Delta_y)`

    which implies: :math:`M Var(W) = 1`

    => :math:`Var(W) = \frac{1}{M}` :marker:`(8)`

Putting ``(6)`` and ``(8)`` together:
    :math:`Var(W) = \frac{1}{N_in}` :marker:`(6)`

    :math:`Var(W) = \frac{1}{N_out}` :marker:`(8)`

    which basically says, *weight variance should depend on both input & output neuron count*

so ``Glorot initialization`` use a harmonic mean to resonate the 2 `[source] <https://stats.stackexchange.com/questions/47590/what-are-good-initial-weights-in-a-neural-network>`_:
    :math:`Var(W_i) = \frac{2}{N_{in} + N_{out}}` :marker:`(9)`

----

Now that we have the correct weight variance, it's easy to derive the value range as just the standard deviation
:math:`\sigma`

    If using ``Xavier Initialization``

    - if sampling weights from a normal distribution N(0, :math:`\sigma`) (standard deviation = :math:`\sigma`):

        :math:`\sigma = \frac{1}{\sqrt{N_in}}` with :math:`mean = 0`, thanks to ``(6)``

        or :math:`W_i \in [ -\frac{1}{\sqrt{N_in}} , \frac{1}{\sqrt{N_in}} ]` :marker:`(*)`

    - if sampling weights from a uniform distribution :math:`U(-\alpha, \alpha)` (standard deviation = :math:`\alpha`):

        because :math:`Var(U(-\alpha, \alpha)) = \frac{(\alpha - (-\alpha))^2}{12}` = :math:`\frac{\alpha^2}{3}`; and
        ``(6)`` suggests here that :math:`Var(U(-\alpha, \alpha)) = \frac{1}{N_in}`

        => :math:`\frac{\alpha^2}{3} = \frac{1}{N_in}` => :math:`\alpha = \sqrt{\frac{3}{N_in}}`

        or :math:`W_i \in [ -\sqrt{\frac{3}{N_in}} , \sqrt{\frac{3}{N_in}} ]` :marker:`(*)`

    If using ``Glorot Initialization``

    - if sampling weights from a normal distribution N(0, :math:`\sigma`) (standard deviation = :math:`\sigma`):

        :math:`\sigma = \sqrt{\frac{2}{N_in + N_out}}` with :math:`mean = 0`, thanks to ``(9)``

        or :math:`W_i \in [ -\sqrt{\frac{2}{N_in + N_out}} , \sqrt{\frac{2}{N_in + N_out}} ]` :marker:`(*)`

    - if sampling weights from a uniform distribution :math:`U(-\alpha, \alpha)`:

        because :math:`Var(U(-\alpha, \alpha)) = \frac{(\alpha - (-\alpha))^2}{12}` = :math:`\frac{\alpha^2}{3}`; and
        ``(9)`` suggests here that :math:`Var(U(-\alpha, \alpha)) = \frac{2}{N_{in} + N_{out}}`

        => :math:`\frac{\alpha^2}{3} = \frac{2}{N_in + N_out}` => :math:`\alpha = \sqrt{\frac{6}{N_in + N_out}}`

        or :math:`W_i \in [ -\sqrt{\frac{6}{N_in + N_out}} , \sqrt{\frac{6}{N_in + N_out}} ]` :marker:`(*)`

----

Plot logistic func :math:`sigmoid(x) = \frac{1}{1 + e^{-\beta x}}`

.. container:: holder

   .. container:: leftside

      .. figure:: static/betas-sigmoid.png
         :width: 400

   .. container:: rightside

        => Observation:

        * smaller beta, more efficient i.e. close to being linear

        * larger beta <= 3.0: gradient descent moves faster (for smaller linear inputs i.e. :math:`\in [-1, 1]`)

----

4.6 Derive Generic Back Propagation
-----------------------------------

We'll derive the back propagation generically for ANN by preserving the term of activation function derivative. Then, we
can plug in any activation func accordingly.

.. container:: holder

   .. container:: leftside

      .. figure:: static/ann-back.png
         :width: 450

   .. container:: rightside

        Neuron names: :math:`A_1, A_2, B_1, ..., Y_2, Y2`

        Computation at each output neuron:
            Net linear sum: :math:`h_{Y_1} = w_11b_1 + w_{21}b_2 + w_{31}b_3`

            Activation: :math:`y_1 = g_{Y_1} = g(h_{Y_1})`

        Loss: :math:`E = \sum_{i=1}_{N} E_i = E_1 + E_2`

2 main driving factors of how the math works out: which loss func & which activation func

.. container:: holder

   .. container:: leftside

      .. figure:: static/ann-back.png
         :width: 450

   .. container:: rightside

        asdfasdf

        * MSE (mean quare error)

            :math:`E_1 = \frac{1}{2} (y_1 - t_1)^2`

            :math:`E_2 = \frac{1}{2} (y_2 - t_2)^2`

        * MSE (mean quare error)

            :math:`E_1 = \frac{1}{2} (y_1 - t_1)^2`

            :math:`E_2 = \frac{1}{2} (y_2 - t_2)^2`

Toward output layer: Tracing loss caused by :math:`w_11`:

    :math:`\frac{\partial E}{\partial w_11} = \frac{\partial E_1}{\partial w_11} = \left( \frac{\partial E_1}{\partial y_1}  \frac{\partial y_1}{\partial h_{Y_1}} \right) \star \frac{\partial h_{Y_1}}{\partial w_11}` :marker:`(4.6.1)`

    1. the left big term, set :math:`\delta_{Y_1} = \frac{\partial E_1}{\partial h_{Y_1}} = \left( \frac{\partial E_1}{\partial g_{Y_1}} \frac{\partial g_{Y_1}}{\partial h_{Y_1}} \right)`

        easily with:

        * :math:`\frac{\partial y_1}{\partial h_{Y_1}} = g\prime (h_{Y_1})` :marker:`(depends on which activation func)`

        * :math:`\frac{\partial E_1}{\partial y_1} = \frac{\partial \left( \frac{1}{2} (y_1 - t_1)^2 \right)}{\partial y_1} = y_1 - t_1` :marker:`(depends on which LOSS func, here is SSE)`

        => :math:`\delta_{Y_1} = \frac{\partial E}{\partial h_{Y_1}} = (y_1 - t_1) \star g\prime (h_{Y_1})` :marker:`(*)`

        :emphaline:`generalize`: for any output neuron :math:`Y_k, k \in 1...N`:

            :math:`\delta_{Y_k} = \frac{\partial E}{\partial h_{Y_k}} = (y_k - t_k) \star g\prime (h_{Y_k})` :marker:`(* 4.6.1)`

    2. The right term:

        :math:`\frac{\partial h_{Y_1}}{\partial w_11} = f_{w_11}\prime (w_11 b_1 + w_21 b_2 + w_31 b_3) = b_1` :marker:`(*)`

        :emphaline:`generalize`: toward output layer, for any :math:`j \in 1...M, k \in 1...N`:

        :math:`\frac{\partial h_{Y_k}}{\partial w_jk} = b_j` :marker:`(*)`

    :emphaline:`Add 1. and 2. together we can finally generalize`:

        toward output layer, for any :math:`j \in 1...M, k \in 1...N`:

            Formula ``(4.6.1)`` = :math:`\frac{\partial E}{\partial w_jk} = \delta_{Y_k} \star b_j` :marker:`(* 4.6.2)`

            Weight update: :math:`w_jk -= \delta_{Y_k} \star b_j` :marker:`(*)`

Toward any hidden layer: Tracing loss caused by :math:`v_21`:

    :math:`\frac{\partial E}{\partial v_21} = (all\ E_x\ mustbe\ included) = \left( \frac{\partial E}{\partial b_1}  \frac{\partial b_1}{\partial h_{B_1}} \right) \star \frac{\partial h_{B_1}}{\partial v_21}` :marker:`(4.6.2)`

    where :math:`\frac{\partial E}{\partial b_1} = \frac{\partial E_1}{\partial b_1} + \frac{\partial E_2}{\partial b_1}`

    1. the left big term, set :math:`\delta_{B_1} = \frac{\partial E}{\partial h_{B_1}} = \left( \frac{\partial E}{\partial b_1}  \frac{\partial b_1}{\partial h_{B_1}} \right)`

        easily with:

        * :math:`\frac{\partial b_1}{\partial h_{B_1}} = g\prime (h_{B_1})` :marker:`(depends on which activation func)`

        * :math:`\frac{\partial E}{\partial b_1} = \frac{\partial E_1}{\partial b_1} + \frac{\partial E_2}{\partial b_1}`

          where each:

          * :math:`\frac{\partial E_1}{\partial b_1} = \left( \frac{\partial E_1}{\partial h_{Y_1}} \right) \frac{\partial h_{Y_1}}{\partial b_1} = \delta_{Y_1} \star \frac{\partial h_{Y_1}}{\partial b_1}`, according to ``(* 4.6.1)``

            and :math:`\frac{\partial h_{Y_1}}{\partial b_1} = f_{b_1}\prime (w_11 b_1 + w_21 b_2 + w_31 b_3) = w_11`

            => :math:`\frac{\partial E_1}{\partial b_1} = \delta_{Y_1} \star w_11`

          * :math:`\frac{\partial E_2}{\partial b_1} = \delta_{Y_2} \star w_12`

          so, :math:`\frac{\partial E}{\partial b_1} = (\delta_{Y_1} \star w_11 + \delta_{Y_2} \star w_12)`

        => :math:`\delta_{B_1} = \frac{\partial E}{\partial h_{B_1}} = (\delta_{Y_1} \star w_11 + \delta_{Y_2} \star w_12) \star g\prime (h_{B_1})` :marker:`(*)`

        :emphaline:`generalize`: for any hidden neuron :math:`B_j, j \in 1...M`:

            :math:`\delta_{B_j} = \frac{\partial E}{\partial h_{B_j}} = \left( \sum_{k=1}^{N}{\delta_{Y_k} \star w_jk} \right) \star g\prime (h_{B_j})` :marker:`(* 4.6.3)`

    2. The right term:

        :math:`\frac{\partial h_{B_1}}{\partial v_21} = f_{v_21}\prime (v_11 a_1 + v_21 a_2) = a_2` :marker:`(*)`

        :emphaline:`generalize`: toward a hidden layer, for any :math:`i \in 1...L, j \in 1...M`:

        :math:`\frac{\partial h_{B_k}}{\partial v_ij} = a_i` :marker:`(*)`

    :emphaline:`Add 1. and 2. together we can finally generalize`:

        Toward a hidden layer, for any :math:`i \in 1...L, j \in 1...M`:

            Formula ``(4.6.2)`` = :math:`\frac{\partial E}{\partial v_ij} = \delta_{B_j} \star a_i` :marker:`(* 4.6.4)`

            Weight update: :math:`v_ij -= \delta_{B_j} \star a_i` :marker:`(*)`

----

The back-propagation algorithm can be seen like a dynamic programming iteration starting from the output layer. Every
neuron except input ones have a :math:`\delta` term to be calculated. After all these :math:`\delta` terms are ready,
weight update between layers can be performed.

* :math:`\delta` term at a neuron k-th in output layer is:

    :math:`\delta_k_{(out)} = \frac{\partial E}{\partial h_k_{(out)}} = (y_k - t_k) \star g\prime (h_k_{(out)})`, :marker:`given by (* 4.6.1)`

* :math:`\delta` term at a neuron j-th in hidden layer (n)-th depends on ALL :math:`\delta` terms in layer (n+1)-th
  and the weights from neuron j-th to layer (n+1)-th; assuming layer (n+1)-th has N neurons:

    :math:`\delta_j_{(n)} = \frac{\partial E}{\partial h_j_{(n)}} = \left( \sum_{k=1}^{N}{\delta_k_{(n+1)} \star w_jk} \right) \star g\prime (h_j_{(n)})` :marker:`given by (* 4.6.3)`

* Once all :math:`\delta` terms are in place, a weight :math:`w_ij` between neuron i-th in layer (n)-th and neuron j-th
  in layer (n+1)-th is smoothen as:

    :math:`w_ij -= \delta_j_{(n+1)} \star x_i_{(n)}` :marker:`given by (* 4.6.2) or (* 4.6.4)`

* no :math:`\delta` term for input layer

:emphaline:`Legends`:

.. container:: math-legends

    * :math:`h_k_{(out)}`   linear sum (from previous neurons & assoc weights) at neuron k-th in output layer

    * :math:`y_k, t_k`      ``predicted value`` and ``actual value (target)`` respectively at neuron k-th in output layer

    * :math:`g\prime (h_k_{(out)})`     derivative of activation func w/ respect to :math:`h_k_{(out)}` at neuron k-th in output layer

    * :math:`h_j_{(n)}`     linear sum (from previous neurons & assoc weights) at neuron j-th in layer (n)-th

    * :math:`g\prime (h_j_{(n)})`       derivative of activation func w/ respect to :math:`h_j_{(n)}` at neuron j-th in layer (n)-th

    * :math:`x_i_{(n)}`     output (activation func result) at neuron i-th in layer (n)-th

----

asdf




