
# Code from Chapter 2 of Machine Learning: An Algorithmic Perspective (2nd Edition)
# by Stephen Marsland (http://stephenmonika.net)

# You are free to use, change, or redistribute the code in any way you wish for
# non-commercial purposes, but please maintain the name of the original author.
# This code comes with no warranty of any kind.

# Stephen Marsland, 2008, 2014

import matplotlib.pyplot as plt
import numpy as np
import math

_beta_0 = 0.5
_beta_1 = 1.0
_beta_2 = 2.0
_beta_3 = 3.0

sigmoid_0 = lambda j: 1 / (1 + math.e**(- _beta_0*j))
sigmoid_1 = lambda j: 1 / (1 + math.e**(- _beta_1*j))
sigmoid_2 = lambda j: 1 / (1 + math.e**(- _beta_2*j))
sigmoid_3 = lambda j: 1 / (1 + math.e**(- _beta_3*j))

x = np.arange(-4., 4., 0.1)

y_0 = sigmoid_0(x)
y_1 = sigmoid_1(x)
y_2 = sigmoid_2(x)
y_3 = sigmoid_3(x)

plt.plot(x, y_0, "-r", label="beta=0.5")
plt.plot(x, y_1, 'black', linewidth=2, label="beta=1.0")
plt.plot(x, y_2, '-g', linewidth=1, label="beta=2.0")
plt.plot(x, y_3, '-b', linewidth=1, label="beta=3.0")
plt.legend(loc="upper left")

plt.xlim(-4., 4.)
plt.ylim(0, 1.)

plt.xlabel('x')
plt.ylabel('sigmoid(x)')
#plt.title('sigmoid w/ different _beta values')

plt.show()
