.. raw:: html

    <style>
        main {
            width: 950px;
        }

        div.holder {
            padding-left: 0;
            overflow: hidden;
        }
        div.leftside {
            padding: 0px;
            float: left;
        }
        div.rightside {
            float: left;
            width: 400px;
        }

        div.container.blue-box {
            border: 1px solid blue;
            padding-left: 20px;
        }

        div.math-legends li > p > .formula:first-child {
            display: inline-block;
            width: 120px;
            text-align: left;
            margin-top: 0;
            margin-bottom: 0;
        }

        div.formula {
            font-size: 18px;
            text-align: left;
            padding-left: 35px;
        }
        div.formula span {
            font-size: 18px;
        }

        p:has(> span.formula) {
            font-size: 18px;
        }

        span.formula, span.formula span {
            font-size: 18px;
        }
        span.formula > span {
            #margin-right: 4px;
        }

        span.stretchy {
            font-size: 28px;
        }

        div:has(> span.environment.align) {
            padding-left: 35px;
            margin: 0;
            display: inline-block;
        }

        .marker {color:#A0C5AC; float: right}

        .emphaline {
            text-decoration: underline;
            font-style: italic;
        }
    </style>

.. role:: marker
.. role:: emphaline

----

4.2.2 Initializing the Weights
------------------------------

.. container:: holder

   .. container:: leftside

      .. figure:: static/forw-neurons.jpg
         :width: 400

   .. container:: rightside

        Number of input neurons to neuron A: :math:`N = 3`

        Input to neuron A:

        - Linear sum: :math:`h = \sum_{i=1}^{3}{w_i x_i} = w_1x_1 + w_2x_2 + w_3x_3` :marker:`(2)`
        - Sigmoid activation: :math:`g(h) = \frac{1}{1 + e^{-x}}` :marker:`(3)`

        *Weights are often initialized to* :math:`[ -\frac{1}{\sqrt{N}} , \frac{1}{\sqrt{N}} ]` - WHY? :marker:`(4)`

----

This method is called ``Xavier Initialization``. Xavier is just 1 of several methods to randomize neuron weights before
training. The particular range ``(4)`` was used for sigmoid and standard normal distribution weight sampling. For other
activation functions and distributions, the chosen ranges can be different.

A great source of explanation is from
`andyljones's blogpost <https://andyljones.tumblr.com/post/110998971763/an-explanation-of-xavier-initialization>`_

1. How to arrive at range ``(4)`` when using sigmoid activation?

    Statistics revision `source: wiki <https://en.wikipedia.org/wiki/Variance#Product_of_independent_variables>`_ :

        If 2 variables X and Y are independent, then:

            :math:`Var(XY) = [E(X)]^2 \star Var(Y) + [E(Y)]^2 \star Var(x) + Var(x) \star Var(Y)`      :marker:`(variance of product)`

            :math:`Var(X + Y) = Var(x) + Var(Y)`                                                       :marker:`(variance of sum)`

        where :math:`E[X]` reads: expected value of variable X

        if X also has zero-mean :math:`\bar{X} = 0`, then :math:`E[X] = 0`

    Wrt formula ``(2)``, any term :math:`W_i X_i` has the following variance:
        :math:`Var(W_i X_i) = [E(X_i)]^2 \star Var(W_i) + [E(W_i)]^2 \star Var(X_i) + Var(X_i) \star Var(W_i)`

    If X and W are both independent, :math:`\bar{X} = 0` and :math:`\bar{W} = 0`, then :math:`E(X_i) = 0` and
    :math:`E(W_i) = 0`, :math:`Var(W_i X_i)` is simplified to:

        :math:`Var(W_i X_i) = Var(X_i) \star Var(W_i)`

    From here, variance of ``h`` in ``(2)`` is computed as:
        :math:`Var(h) = Var(w_1x_1) + Var(w_2x_2) + Var(w_3x_3) = N \star Var(W) \star Var(X)`, :marker:`(5)`

        `N = 3` in this

    Because we want :math:`Var(h) = Var(X)`, this condition must satisify: :math:`N*Var(W) = 1`, implies:
        :math:`Var(W) = \frac{1}{N}` :marker:`(6)`

        hence, :math:`W_i \in [ -\frac{1}{\sqrt{N}} , \frac{1}{\sqrt{N}} ]` :marker:`(Q.E.D)`

    In Pytorch ANN impl, init weights also follow this rule, see
    `source <https://stackoverflow.com/questions/49816627/what-is-the-default-weight-initializer-for-conv-in-pytorch>`_

2. In words, now we see why it's important to:

    a. Choose features that are independent from one another. This help make all the terms in ``(2)`` independent.

    b. Scale every feature :math:`X_i` to a a specific distribution with :math:`Var(X) = 1, \bar{X} = 0`.

    As all input values started in range :math:`[-1, 1]`, condition ``(Q.E.D)`` ensures every neuron linear output in the
    next layer also lies in :math:`[-1, 1]`, and sigmoid for any input in this range is small enough and lies in
    :math:`(-1, 1)`. As a result, linear input to any neuron across al layers remain in a small reasonable range, allow
    sigmoid activation to make effective weight update.

3.  Why choosing the range :math:`[-1, 1]` for effective weight update?

    This we have to look at the shape of sigmoid function
    `image source <https://mathworld.wolfram.com/images/eps-svg/SigmoidFunction_701.svg>`_:

    .. container:: holder

       .. container:: leftside

          .. figure:: static/sigmoid-shape.png

       .. container:: rightside

            x-axis represents linear input to a neuron;

            y-axis gives the relevant sigmoid output;

    When the linear input :math:`\in [-1, 1]`, its sigmoid shape is almost linear and has significant
    derivative (gradient vector). This means a gradient descent (moving in the steepest direction) is effective, making
    a weight update meaningful. If linear input is too large (< -4 or > 4), the sigmoid shape is saturated (almost flat out)
    at these positions. So a gradient descent is very small, making it move very slowly. This translates to slow
    and costly leaning.

----

While ``Xavier Initialization`` only depends on # of input neurons, ``Glorot initialization`` is another method which
takes into account # of both input & output neurons. The argument applies in the back-propagation step.

.. container:: holder

   .. container:: leftside

      .. figure:: static/back-neurons.jpg
         :width: 400

   .. container:: rightside

        Number of output neurons from neuron B: :math:`M = 2`

        Let's ``L`` denote the loss function, set :math:`\Delta_{y1} = \frac{\delta_L}{\delta_{y_1}}` (derivative of loss with
        respect to y1), :math:`\Delta_{B} = \frac{\delta_L}{\delta_B}` (derivative of loss with respect to B). Thanks to an
        already proven math, we have:

            :math:`\Delta_{B} = \sum_{i=1}^{2}{w_i \Delta_{y_i}} = w_1 \Delta_{y_1} + w_2 \Delta_{y_2}` :marker:`(7)`

Formula ``(7)`` is similar to ``(2)``. As how we go from ``(2)`` to ``(5)``, we can also go from ``(7)`` to:
    :math:`Var(\Delta_{B}) = Var(w_1\Delta_{y_1}) + Var(w_2\Delta_{y_2}) = M \star Var(W) \star Var(\Delta_y)`,
    :marker:`M = 2 in this example`

In back-propagation, it's argued that `we should maintain change to input & output neurons on the same range`,
which means we want:

    :math:`Var(\Delta_{B}) = Var(\Delta_y)`

    which implies: :math:`M \star Var(W) = 1`

    => :math:`Var(W) = \frac{1}{M}` :marker:`(8)`

Putting ``(6)`` and ``(8)`` together:
    :math:`Var(W) = \frac{1}{N_in}` :marker:`(6)`

    :math:`Var(W) = \frac{1}{N_out}` :marker:`(8)`

    which basically says, *weight variance should depend on both input & output neuron count*

so ``Glorot initialization`` use a harmonic mean to resonate the 2 `[source] <https://stats.stackexchange.com/questions/47590/what-are-good-initial-weights-in-a-neural-network>`_:
    :math:`Var(W_i) = \frac{2}{N_{in} + N_{out}}` :marker:`(9)`

----

Now that we have the correct weight variance, it's easy to derive the value range as just the standard deviation
:math:`\sigma`

    If using ``Xavier Initialization``

    - if sampling weights from a ``normal distribution`` N(0, :math:`\sigma`) (standard deviation = :math:`\sigma`):

        :math:`\sigma = \frac{1}{\sqrt{N_in}}` with :math:`mean = 0`, thanks to ``(6)``

        .. container:: blue-box

            or :math:`W_i \in [ -\frac{1}{\sqrt{N_in}} , \frac{1}{\sqrt{N_in}} ]` :marker:`(*)`

    - if sampling weights from a ``uniform distribution`` :math:`U(-\alpha, \alpha)` (standard deviation = :math:`\alpha`):

        because :math:`Var(U(-\alpha, \alpha)) = \frac{(\alpha - (-\alpha))^2}{12}` = :math:`\frac{\alpha^2}{3}`; and
        ``(6)`` suggests here that :math:`Var(U(-\alpha, \alpha)) = \frac{1}{N_in}`

        => :math:`\frac{\alpha^2}{3} = \frac{1}{N_in}` => :math:`\alpha = \sqrt{\frac{3}{N_in}}`

        .. container:: blue-box

            or :math:`W_i \in [ -\sqrt{\frac{3}{N_in}} , \sqrt{\frac{3}{N_in}} ]` :marker:`(*)`

    If using ``Glorot Initialization``

    - if sampling weights from a ``normal distribution`` N(0, :math:`\sigma`) (standard deviation = :math:`\sigma`):

        :math:`\sigma = \sqrt{\frac{2}{N_in + N_out}}` with :math:`mean = 0`, thanks to ``(9)``

        .. container:: blue-box

            or :math:`W_i \in [ -\sqrt{\frac{2}{N_in + N_out}} , \sqrt{\frac{2}{N_in + N_out}} ]` :marker:`(*)`

    - if sampling weights from a ``uniform distribution`` :math:`U(-\alpha, \alpha)`:

        because :math:`Var(U(-\alpha, \alpha)) = \frac{(\alpha - (-\alpha))^2}{12}` = :math:`\frac{\alpha^2}{3}`; and
        ``(9)`` suggests here that :math:`Var(U(-\alpha, \alpha)) = \frac{2}{N_{in} + N_{out}}`

        => :math:`\frac{\alpha^2}{3} = \frac{2}{N_in + N_out}` => :math:`\alpha = \sqrt{\frac{6}{N_in + N_out}}`

        .. container:: blue-box

            or :math:`W_i \in [ -\sqrt{\frac{6}{N_in + N_out}} , \sqrt{\frac{6}{N_in + N_out}} ]` :marker:`(*)`

----

Plot logistic func :math:`sigmoid(x) = \frac{1}{1 + e^{-\beta x}}`

.. container:: holder

   .. container:: leftside

      .. figure:: static/betas-sigmoid.png
         :width: 400

   .. container:: rightside

        => Observation:

        * smaller beta, more efficient i.e. close to being linear

        * larger beta <= 3.0: gradient descent moves faster (for smaller linear inputs i.e. :math:`\in [-1, 1]`)

----

4.6 Derive Generic Back Propagation
-----------------------------------

We'll derive the back propagation generically for ANN by preserving the term of activation function derivative. Then, we
can plug in any activation func accordingly.

.. container:: holder

   .. container:: leftside

      .. figure:: static/ann-back.png
         :width: 450

   .. container:: rightside

        Neuron names: :math:`A_1, A_2, B_1, ..., Y_2, Y2`

        Computation at each output neuron:
            Net linear sum: :math:`h_{Y_1} = w_{11}b_1 + w_{21}b_2 + w_{31}b_3`

            Activation: :math:`y_1 = g_{Y_1} = g(h_{Y_1})`

        Loss: :math:`E = \sum_{i=1}^N E_i = E_1 + E_2`

2 main driving factors of how the math works out: which loss func & which activation func

.. container:: holder

   .. container:: leftside

        Which loss func:

        * MSE (mean quare error)

            :math:`E_1 = \frac{1}{2} (y_1 - t_1)^2`

            :math:`E_2 = \frac{1}{2} (y_2 - t_2)^2`

        * Cross-entropy error func:

            :math:`E_1 = -t_1 \star ln(y_1)`

            :math:`E_2 = -t_2 \star ln(y_2)`

   .. container:: rightside

        Which activation func:

        * Linear:

            :math:`y_k = g_{Y_k} = g(h_{Y_k}) = h_{Y_k}`

            => :math:`g\prime (h_{Y_k}) = 1, (g\prime w/\ respect\ to\ h_{Y_k})`

        * Sigmoid:

            :math:`y_k = g_{Y_k} = g(h_{Y_k}) = \frac{1}{1 + exp(-\beta \star h_{Y_k})}`

            => :math:`g\prime (h_{Y_k}) = y_k (1 - y_k)`

        * Soft-max:

            :math:`y_k = g_{Y_k} = g(h_{Y_k}) = \frac{exp(h_k)}{\sum_{r=1}^N exp(h_r)}`

            => :math:`\frac{\partial g(h_{Y_k})}{\partial h_{Y_r}} = @`
            `Jacobian matrix <https://towardsdatascience.com/derivative-of-the-softmax-function-and-the-categorical-cross-entropy-loss-ffceefc081d1>`_ (:math:`k, r \in 1..N`)

:emphaline:`The essence of measuring affect of any weight` :math:`w_{ij}` on loss `E` started with this derivative chain:

    .. container:: blue-box

        (Example for weight :math:`w_{11}`)

        :math:`\frac{\partial E}{\partial w_{11}} = \frac{\partial E}{\partial h_{Y_1}} \star \frac{\partial h_{Y_1}}{\partial w_{11}}` :marker:`(4.6.1)`

    * Term :math:`\frac{\partial h_{Y_1}}{\partial w_{11}}` is the easiest one. Commonly the net output `h` to any neuron
      is just a linear sum from previous layer & assoc weights, this term becomes just the input scalar from the the
      connected neuron in the previous layer:

        :math:`h_{Y_1} = w_{11} b_1 + b_21 b_2 + w_{31} b_3 + bias`

        => :math:`\frac{\partial h_{Y_1}}{\partial w_{11}} = b1`

    * Strategically :math:`\frac{\partial E}{\partial h_{Y_1}}` is set as a :math:`\delta_{Y_1}` term - the main contributor and
      differentiator among activation & loss funcs in use.

      Once :math:`\delta_{Y_k}` term for an output neuron is computed, :math:`\delta` terms for hidden layer neurons
      (layer n, n-1, n-2, etc) are computed recursively through this chain:

        :math:`\delta_{Y_k}` -> :math:`\delta_{j}^{(n)}` -> :math:`\delta_{i}^{(n-1)}` -> ...

      This is exactly what back-propagation is doing. It's very much like a dynamic programming algorithm.

    * Then a particular weight :math:`w_{ij}` between neuron i-th in layer (n)-th and neuron j-th in layer (n+1)-th is
      smoothen as:

        .. container:: blue-box

            :math:`w_{ij} -= \frac{\partial E}{\partial w_{ij}} = \delta_j^{(n+1)} \star y_i^{(n)}` (replace :math:`y_i^{(n)}` by :math:`x_i` if neuron i-th is in input layer)

    * There's no :math:`\delta` term for input neurons

:emphaline:`For Sigmoid output & MSE loss`: compute :math:`\delta` terms

    Back-propagation from output layer:

    .. math::

        \delta_{Y_1} &= \frac{\partial E}{\partial h_{Y_1}} = \frac{\partial (E_1 + E_2)}{\partial h_{Y_1}} & \\
                     &= \frac{\partial E_1}{\partial h_{Y_1}} \ (because\ sigmoid\ neuron\ outputs\ are\ independent\ from\ one\ another,\ only\ 1\ E\ term\ applies) & \\
                     &= \left( \frac{\partial E_1}{\partial y_1} \right) \star \left( \frac{\partial y_1}{\partial h_{Y_1}} \right)  & \\
                     &= \left( \frac{\partial \left( \frac{1}{2} (y_1 - t_1)^2 \right)}{\partial y_1} \right) \star sigmoid\prime_{Y_1} & \\
        \delta_{Y_1} &= (y_1 - t_1) \star y_1(1 - y_1) &

    => back-propagation from an output neuron :math:`Y_k, k \in 1...N`:

        .. container:: blue-box

            :emphaline:`for sigmoid/MSE loss`:
            :math:`\delta_{Y_k} = \frac{\partial E}{\partial h_{Y_k}} = (y_k - t_k) \star y_k(1 - y_k)` :marker:`(* 4.6.1)`

:emphaline:`For Soft-max output & Cross-entropy loss (ln)`: compute :math:`\delta` terms
(helping sources: `crappy <https://alexcpn.github.io/html/NN/ml/8_backpropogation_full/>`_, `stackexchange <https://stats.stackexchange.com/questions/235528/backpropagation-with-softmax-cross-entropy>`_)

    Back-propagation from output layer:

    .. math::

        \delta_{Y_1} &= \frac{\partial E}{\partial h_{Y_1}} = \frac{\partial (E_1 + E_2)}{\partial h_{Y_1}} \ (all\ E\ terms\ included\ because\ soft-max\ neuron\ outputs\ affect\ one\ another\ due\ to\ the\ common\ denominator) \\
                     &= - \frac{\partial [t_1 \star ln(y_1) + t_2 \star ln(y_2)]}{\partial h_{Y_1}} \\
                     &= - \left( t_1 \frac{\partial ln(y_1)}{\partial h_{Y_1}} + t_2 \frac{\partial ln(y_2)}{\partial h_{Y_1}} \right) \\
                     &= - \left( t_1 \star \frac{1}{y_1} \star \frac{\partial y_1}{\partial h_{Y_1}} + t_2 \star \frac{1}{y_2} \star \frac{\partial y_2}{\partial h_{Y_1}} \right)

    , here recalled that:

        :math:`\frac{\partial y_1}{\partial h_{Y_1}}`: derivative of soft-max where `Kronecker delta <https://en.wikipedia.org/wiki/Kronecker_delta>`_ :math:`\delta_ij = 1` (because i=j=1)

        :math:`\frac{\partial y_2}{\partial h_{Y_1}}`: derivative of soft-max where Kronecker delta :math:`\delta_ij = 0` (because i=2 != j=1)

    , therefore:

    .. math::

        \delta_{Y_1} &= - \left( t_1 \star \frac{1}{y_1} \star \frac{\partial y_1}{\partial h_{Y_1}} + t_2 \star \frac{1}{y_2} \star \frac{\partial y_2}{\partial h_{Y_1}} \right) \\
                     &= - \left( t_1 \star \frac{1}{y_1} \star y_1(1 - y_1) \right) - \left( t_2 \star \frac{1}{y_2} \star y_2(0 - y_1) \right) \\
                     &= - t_1 (1 - y_1) - t_2 (0 - y_1) = -t_1 + t_1 \star y_1 + t_2 \star y_1 \\
                     &= -t_1 + (t_1 + t_2) y_1 & (assuming\ 1-hot-encoding\ gives: t_1 + t_2 = 1) \\
        \delta_{Y_1} &= y_1 - t_1

    => back-propagation from an output neuron :math:`Y_k, k \in 1...N`:

        .. container:: blue-box

            :emphaline:`for soft-max/cross-entropy loss`:
            :math:`\delta_{Y_k} = \frac{\partial E}{\partial h_{Y_k}} = y_k - t_k` :marker:`(* 4.6.1)`

:emphaline:`Back-propagation from hidden layers, for either sigmoid/MSE or soft-max/cross-entropy`:

    :math:`\delta` terms for hidden layers are typically the same among ANNs regardless of what output activation & LOSS
    func in use. Reason is because once the algorithm reaches hidden layers, their :math:`\delta` terms no longer depend
    directly on those funcs. Instead, these terms are computed recursively from `\delta`'s of the an immediate layer
    to its right. As :math:`\delta` terms in the output layer was ready, they propagate back all the way to the beginning
    of the network.

    .. math::

        \delta_{B_1} &= \frac{\partial E}{\partial h_{B_1}} = \left[ \frac{\partial E}{\partial b_1} \right] \star \left( \frac{\partial b_1}{\partial h_{B_1}} \right) & \\
                     &= \left[ \frac{\partial E}{\partial h_{Y_1}} \star \frac{\partial h_{Y_1}}{\partial b_1} + \frac{\partial E}{\partial h_{Y_2}} \star \frac{\partial h_{Y_2}}{\partial b_1} \right] \star \left( \frac{\partial b_1}{\partial h_{B_1}} \right) & \\
                     &= [ \delta_{Y_1} \star f_{b_1}\prime (w_{11} b_1 + w_{21} b_2 + w_{31} b_3) \ +\  \delta_{Y_2} \star f_{b_1}\prime (w_{12} b_1 + w_{22} b_2 + w_{32} b_3) ] \star \frac{\partial b_1}{\partial h_{B_1}} \\
        \delta_{B_1} &= [ \delta_{Y_1} \star w_{11} + \delta_{Y_2} \star w_{12} ] \star \frac{\partial b_1}{\partial h_{B_1}}

    => back-propagation from a hidden neuron i-th in layer (n)-th depends on 2 things:

        * ALL N :math:`\delta` terms in layer (n+1)-th with respective weights from neuron i-th to this layer

        * Activation function used at neuron i-th in layer (n)-th

        .. container:: blue-box

            :emphaline:`for hidden layers`:
            :math:`\delta_i^{(n)} = \left[ \sum_{j=1}^N \delta_j^{(n+1)} w_{ij} \right] \star \frac{\partial g_i^{(n)}}{\partial h_i^{(n)}}`

----

The back-propagation algorithm can be seen like a dynamic programming iteration starting from the output layer. Every
neuron except input ones have a :math:`\delta` term to be calculated. After all these :math:`\delta` terms are ready,
weight update between layers can be performed.

:emphaline:`Legends`:

.. container:: math-legends

    * :math:`h_k^{(out)}`   linear sum (from previous neurons & assoc weights) at neuron k-th in output layer

    * :math:`y_k, t_k`      ``predicted value`` and ``actual value (target)`` respectively at neuron k-th in output layer

    * :math:`g\prime (h_k^{(out)})`     derivative of activation func w/ respect to :math:`h_k^{(out)}` at neuron k-th in output layer

    * :math:`h_j^{(n)}`     linear sum (from previous neurons & assoc weights) at neuron j-th in layer (n)-th

    * :math:`g\prime (h_j^{(n)})`       derivative of activation func w/ respect to :math:`h_j^{(n)}` at neuron j-th in layer (n)-th

    * :math:`x_i^{(n)}`     output (activation func result) at neuron i-th in layer (n)-th

----

asdf




