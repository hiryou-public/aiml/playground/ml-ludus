import torch

print('is_available:', torch.cuda.is_available())

if torch.cuda.is_available():
    print('device_count =', torch.cuda.device_count())
    print('current_device :', torch.cuda.current_device())
    print('device[1] :', torch.cuda.get_device_properties(1))

